package com.giorgirusia;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@Slf4j
@RunWith(Parameterized.class)
public class AppTest {

    private final String[] inputArgs;

    public AppTest(String[] inputArgs) {
        this.inputArgs = inputArgs;
    }

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null},
                {new String[]{"5"}},
                {new String[]{"6", "2", "32", "1", "31", "45", "7", "5", "10", "9"}},
                {new String[]{"5", "1", "2", "8", "7", "4", "3", "10", "6", "9", "12"}},
                {new String[]{"5", "2", "8", "1", "7.2", "4", "3", "0.5", "6", "9"}}
        });
    }

    @Test
    public void testApp() {
        try {
            App.main(inputArgs);
        } catch (Exception e) {
            log.error("Unexpected exception: {}", e.getMessage());
            fail("Unexpected exception: " + e.getMessage());
        }
    }
}
