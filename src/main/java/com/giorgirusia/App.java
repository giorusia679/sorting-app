package com.giorgirusia;

import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;

@Slf4j
public class App {

    public static void main(String[] args) {
        try {
            validateArguments(args);

            int[] numbers = convertToNumbers(args);

            Arrays.sort(numbers);

            log.info("Sorted values: {}", Arrays.toString(numbers));
        } catch (IllegalArgumentException e) {
            log.error("Error: {}", e.getMessage());
        }
    }

    private static void validateArguments(String[] args) {
        if (args == null) {
            throw new IllegalArgumentException("Caught null as an argument.");
        }

        if (args.length > 10) {
            throw new IllegalArgumentException("Too many arguments. Please provide up to 10 integer values.");
        }
    }

    private static int[] convertToNumbers(String[] args) {
        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid input. Please provide ONLY valid integer values.");
        }
        return numbers;
    }
}
